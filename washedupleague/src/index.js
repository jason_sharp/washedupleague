import React from 'react';
import ReactDOM from 'react-dom';
import logo from './WULogo.png'
import './index.css';
import { render } from '@testing-library/react';
import ListSignedUpPlayers from './PlayerList.js';
import SummaryWidget from "./SummaryWidget.js";

const userlist = [
  {
      id: 1,
      name: 'Jason',
      rank: '23',
      record: '2-0'
  },
  {
      id: 2,
      name: 'Dennis',
      rank: '24',
      record: '1-2' 
  },
  {
      id: 3,
      name: 'Ryan',
      rank: '26',
      record: '2-2' 
  }
]

function Info(props){
    return(
        <div id='Info'>This is the info page</div>
    );
}

function Rules(props){
    return(
        <div id='Info'>This is the rules page</div>
    );
}

function PreviousSeason(props){
    return(
        <div id='Info'>This is the Previous Season page</div>
    );
}


function NavButton(props){
    return(
        <button onClick={props.onClick}>{props.buttonName}</button>
    );

}

class NavBar extends React.Component{

    render(){

        let navBarNames = ["Players","Info","Rules","Past Seasons"];

        const navBarLinks = navBarNames.map((item)=>{
            return <NavButton onClick={()=>this.props.onClick(item)} buttonName={item} />
        })

        return(
            <div id="navbar">
                {navBarLinks}
            </div>
        );
    }
}

class Main extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            current: "Players"
        }
    }

    handleClick(buttonName){
        this.setState({current: buttonName})
    }

    renderMainContent(){
        switch(this.state.current){
            case "Players":
                return <ListSignedUpPlayers list={userlist} />
                break;
            case "Info":
                return <Info />
                break;
            case "Rules":
                return <Rules />
                break;
            case "PreviousSeason":
                return <PreviousSeason />
                break;
            default:
                return <ListSignedUpPlayers list={userlist} />
                
        }
    }

    render(){

        return (
            <div>
                <div id='header'>
                    <div className='header-content'>
                        <div className='logo-area'>
                            <img className='image' src={logo} alt='wulogo'></img>
                            <h1>Soulglider's Site</h1>
                        </div>
                        <button className='Button'>Login</button>
                    </div>
                </div>
                <SummaryWidget loggedIn={false} signedUp={false} status={0} startDate={10} signedUpPlayers={5} maxPlayers ={10}/>
                <div id='league-title'>
                    <h1>Washed up league season 4</h1>
                </div>
                <NavBar onClick={(buttonName)=>this.handleClick(buttonName)} />
                {this.renderMainContent()}
            </div>
        );
    }
}

ReactDOM.render(<Main />, document.getElementById("root"));