import React from 'react';

function PlayerInfoListDisplay(props){
    return(
        <tr key={props.list.id}>
            <td>{props.list.id}</td>
            <td>{props.list.name}</td> 
            <td>{props.list.rank}</td> 
            <td>{props.list.record}</td>
        </tr>
    );
}

class ListSignedUpPlayers extends React.Component{

    render(){

        const players = this.props.list.map((item) => {
            return <PlayerInfoListDisplay list={item} />
        })

        return(
            <div>
            <table id='players'>
                <tbody>
                    <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Rank</th>
                        <th>Record</th>
                    </tr>
                    {players}
                </tbody>
            </table>
            </div>
        );
    }
}

export default ListSignedUpPlayers;