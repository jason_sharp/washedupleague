import React, { useState } from 'react';
import DateTimePicker from 'react-datetime-picker'
import { format } from "date-fns";


class SummaryWidget extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            status: this.props.status, //0 = not started, 1 = started, 2 = ended
            startDate: null,
            isLoggedIn: false,
            isAdmin: false,
            isSignedUp: false
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    } 


    handleChange(event){
        this.setState({startDate: event});
    }

    handleSubmit(event){
        alert('There was submission');
        event.preventDefault();
    }

    handleLoginButton(){
        //not logged in, logs you in
        if(!this.state.isLoggedIn){
            this.setState({
                isLoggedIn: true
            });
        } else if(!this.state.isSignedUp){
            this.setState({
                isSignedUp:true
            });
        } else{
            this.setState({
                isSignedUp: false
            });
        }

    }

    onAdminLoginToggle(){
        this.setState({
            isAdmin: !this.state.isAdmin
        })
    }


    renderSignupButton(){

        let buttonText = '';
        let buttonClass = "";
        if(this.state.status > 0){ // if it's already started (status 1 or 2), then no button
            return;
        } else if(!this.state.isLoggedIn){
            buttonText = 'Login to Join';
            buttonClass = "login";
        } else if(!this.state.isSignedUp){
            buttonText = 'Join League';
            buttonClass = "signup";
        } else{
            buttonText = 'Leave League';
            buttonClass = "quit";
        }

        return(
            <button className={"SummaryWidgetButton " + buttonClass} onClick={()=>{this.handleLoginButton()}}>{buttonText}</button>
        );
    }

    renderStartDate(){
        if(this.state.isLoggedIn && this.state.isAdmin){
            return (<DateTimePicker onChange={this.handleChange} value={this.state.startDate} />);
        } else{
            return (<div>{this.state.startDate ? format(this.state.startDate, "MMMM do, yyyy H:mma") : "No Date Yet"}</div>);
        }
    }

    render(){

        let Status = '';
        if(this.state.status === 0){
            Status = 'Signups Open';
        } else if (this.state.status === 1){
            Status = 'On going';
        } else{
            Status = 'Finished';
        }

        return(
            <div id="SummaryWidget">
                <div id="summarycontent">
                    <h1>Washed Up League S4</h1>
                    <div className="calendar">Start Date: {this.renderStartDate()} </div>
                    <h3>League Length: 4 weeks + playoffs</h3>
                    <h3>Status: {Status}</h3>
                    <h3>Players: {this.props.signedUpPlayers}/{this.props.maxPlayers}</h3>
                    {this.renderSignupButton()}
                    <button onClick={()=>{this.onAdminLoginToggle()}} className="admin-login-toggle">Admin Login</button>
                </div>
            </div>
        );
    }
}

export default SummaryWidget;